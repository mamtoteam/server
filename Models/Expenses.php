<?php
/**
 * Created by PhpStorm.
 * User: cysiu
 * Date: 26.11.2018
 * Time: 16:25
 */

namespace Model;

use controllers\AppController;
use PDO;
use PDOException;

class Expenses extends Database
{

    /**
     * Expenses constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getMoneyOnAllAccounts($id_user)
    {
        $pdo = $this->connect();

        $sql = "SELECT SUM(amount) AS COMON FROM " . $this->tabPrefix("expense") . " NATURAL JOIN " . $this->tabPrefix("money_account") . " WHERE id_user = :id";

        $stm = $pdo->prepare($sql);
        $stm->bindParam(":id", $id_user, PDO::PARAM_INT);
        $stm->execute();

        $res = $this->returnOnlyOneResult($stm->fetchAll(), 'COMON');

        return $res == null ? 0 : $res;
    }

    public function getMoneyAndNamesAccount($id_user)
    {
        $pdo = $this->connect();

        $sql = "SELECT name, SUM(amount) AS COMON 
        FROM " . $this->tabPrefix("expense") . " RIGHT JOIN " . $this->tabPrefix("money_account") . " ON " . $this->tabPrefix("expense") . ".id_m_acc = " . $this->tabPrefix("money_account") . ".id_m_acc
         WHERE id_user = :id GROUP BY name";

        $stm = $pdo->prepare($sql);
        $stm->bindParam(":id", $id_user, PDO::PARAM_INT);
        $stm->execute();

        return $stm->fetchAll();
    }

    public function getCategoryListExpense($id_user)
    {
        $pdo = $this->connect();

        $sql = "SELECT cat.id_category, cat.name, col.color FROM " . $this->tabPrefix("category") .
            " cat LEFT JOIN " . $this->tabPrefix("color") . " col  ON cat.id_color = col.id_color " .
            "WHERE cat.id_user = :id AND cat.is_income = 0";

        $stm = $pdo->prepare($sql);
        $stm->bindParam(":id", $id_user, PDO::PARAM_INT);
        $stm->execute();

        return $stm->fetchAll();
    }

    public function getCategoryListIncome($id_user)
    {
        $pdo = $this->connect();

        $sql = "SELECT cat.id_category, cat.name, col.color FROM " . $this->tabPrefix("category") .
            " cat LEFT JOIN " . $this->tabPrefix("color") . " col  ON cat.id_color = col.id_color " .
            "WHERE cat.id_user = :id AND cat.is_income = 1";

        $stm = $pdo->prepare($sql);
        $stm->bindParam(":id", $id_user, PDO::PARAM_INT);
        $stm->execute();

        return $stm->fetchAll();
    }

    public function getSubCategoryList($id_category)
    {
        $result = array();
        $temp = $this->getDefSubcategory($id_category);
        $result[0] = $temp[0];

        $temp = $this->getSubcategoryListWOutDef($id_category);

        return array_merge_recursive($result, $temp);
    }

    private function getDefSubcategory($id_category)
    {
        $pdo = $this->connect();
        $sql = "SELECT sub.id_sub_cat, sub.name, col.color, cat.name AS cat_name, cat.id_category FROM " . $this->tabPrefix("subcategory") . " sub " .
            "LEFT JOIN " . $this->tabPrefix("category") . " cat ON sub.id_category = cat.id_category " .
            "LEFT JOIN " . $this->tabPrefix("color") . " col ON cat.id_color = col.id_color " .
            "WHERE sub.id_category = :id_cat AND cat.id_def_subcategory = sub.id_sub_cat";

        $stm = $pdo->prepare($sql);
        $stm->bindParam(":id_cat", $id_category, PDO::PARAM_INT);
        $stm->execute();

        return $stm->fetchAll();
    }

    private function getSubcategoryListWOutDef($id_categoty)
    {
        $pdo = $this->connect();
        $sql = "SELECT sub.id_sub_cat, sub.name, col.color, cat.name AS cat_name, cat.id_category FROM " . $this->tabPrefix("subcategory") . " sub " .
            "LEFT JOIN " . $this->tabPrefix("category") . " cat ON sub.id_category = cat.id_category " .
            "LEFT JOIN " . $this->tabPrefix("color") . " col ON cat.id_color = col.id_color " .
            "WHERE sub.id_category = :id_cat AND cat.id_def_subcategory <> sub.id_sub_cat " .
            "ORDER BY sub.name";

        $stm = $pdo->prepare($sql);
        $stm->bindParam(":id_cat", $id_categoty, PDO::PARAM_INT);
        $stm->execute();

        return $stm->fetchAll();
    }

    public function getAccountsList($id_user)
    {
        $pdo = $this->connect();

        $sql = "SELECT id_m_acc, name 
        FROM " . $this->tabPrefix("money_account") . " WHERE id_user = :id AND visible = 1 GROUP BY name";

        $stm = $pdo->prepare($sql);
        $stm->bindParam(":id", $id_user, PDO::PARAM_INT);
        $stm->execute();

        return $stm->fetchAll();
    }

    public function addExpense($id_m_acc, $amount, $date, $note, $id_subcategory, $type)
    {

        try {
            $pdo = $this->connect();

            $sql = "INSERT INTO " . $this->tabPrefix("expense") . " (id_m_acc, amount, date, note, id_subcategory, type)" .
                " VALUES (:acc, :amount, :datee, :note, :subcat, :typee)";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":acc", $id_m_acc, PDO::PARAM_INT);
            $stm->bindParam(":amount", $amount, PDO::PARAM_STR);
            $stm->bindParam(":datee", $date, PDO::PARAM_STR);
            $stm->bindParam(":note", $note, PDO::PARAM_STR);
            $stm->bindParam(":subcat", $id_subcategory, PDO::PARAM_INT);
            $stm->bindParam(":typee", $type, PDO::PARAM_INT);

            return $stm->execute();

        } catch (PDOException $e) {
            $eadmin = ADMIN_ERROR_INFO ? $e->getMessage() : '';
            $app = new AppController();
            $app->throwErrorPage(3, $eadmin);
            exit();
        }
    }

    public function addTransfer($id_m_acc_source, $id_m_acc_dest, $amount, $date)
    {
        $pdo = $this->connect();

        try {
            $pdo->beginTransaction();
            $sql = "SELECT name FROM " . $this->tabPrefix("money_account") . " where id_m_acc = :id_acc";
            $stm = $pdo->prepare($sql);
            $stm->bindParam(":id_acc", $id_m_acc_source, PDO::PARAM_INT);
            $stm->execute();

            $res = $stm->fetch();
            $accName[0] = $res['name'];

            $stm->bindParam(":id_acc", $id_m_acc_dest, PDO::PARAM_INT);
            $stm->execute();

            $res = $stm->fetch();
            $accName[1] = $res['name'];

            $sql = "INSERT INTO " . $this->tabPrefix("expense") . " (id_m_acc, amount, date, note, type)" .
                " VALUES (:acc, :amount, :datee, :note, :type)";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":acc", $id_m_acc_dest, PDO::PARAM_INT);
            $stm->bindParam(":amount", $amount, PDO::PARAM_STR);
            $stm->bindParam(":datee", $date, PDO::PARAM_STR);
            $stm->bindValue(":note", $accName[0] . " &rarr; " . $accName[1], PDO::PARAM_STR);
            $stm->bindValue(":type", 3, PDO::PARAM_INT);
            $stm->execute();

            $amount = $amount * -1;

            $stm->bindParam(":acc", $id_m_acc_source, PDO::PARAM_INT);
            $stm->bindParam(":amount", $amount, PDO::PARAM_STR);
            $stm->bindParam(":datee", $date, PDO::PARAM_STR);
            $stm->bindValue(":note", $accName[0] . " &rarr; " . $accName[1], PDO::PARAM_STR);
            $stm->bindValue(":type", 2, PDO::PARAM_INT);
            $stm->execute();

            $pdo->commit();

            return true;

        } catch (PDOException $e) {
            $pdo->rollBack();
            $eadmin = ADMIN_ERROR_INFO ? $e->getMessage() : '';
            $app = new AppController();
            $app->throwErrorPage(3, $eadmin);
            exit();
        }
    }

    public function getExpenseList($id_user, $limit = 500)
    {
        $pdo = $this->connect();

        $sql = "SELECT * FROM " . $this->tabPrefix("expense_list") . " WHERE id_user = :id " .
            "ORDER BY date DESC LIMIT :limit";

        $stm = $pdo->prepare($sql);
        $stm->bindParam(":id", $id_user, PDO::PARAM_INT);
        $stm->bindParam(":limit", $limit, PDO::PARAM_INT);
        $stm->execute();

        return $stm->fetchAll();
    }

    public function getFilteredExpenses($id_user, $dateFrom, $dateTo, $account, $category)
    {
        $pdo = $this->connect();

        $sql = "SELECT * FROM " . $this->tabPrefix("expense_list") . " WHERE id_user = :id_user ";

        if($dateFrom != null && $dateTo != null)
            $sql = $sql . " AND date BETWEEN :dateFrom AND :dateTo ";

        if($account != null)
            $sql = $sql . " AND id_m_acc = :acc ";

        if($category != null)
            $sql = $sql . " AND id_category = :cat ";

        $stm = $pdo->prepare($sql);
        $stm->bindParam(":id_user", $id_user, PDO::PARAM_INT);

        if($dateFrom != null && $dateTo != null){
            $stm->bindParam(":dateFrom", $dateFrom, PDO::PARAM_STR);
            $stm->bindParam(":dateTo", $dateTo, PDO::PARAM_STR);
        }

        if($account != null)
            $stm->bindParam(":acc", $account, PDO::PARAM_INT);

        if($category != null)
            $stm->bindParam(":cat", $category, PDO::PARAM_INT);

        $stm->execute();

        return $stm->fetchAll();
    }
}