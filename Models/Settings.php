<?php
/**
 * Created by PhpStorm.
 * User: cysiu
 * Date: 30.12.2018
 * Time: 16:17
 */

namespace Model;

use controllers\AppController;
use PDO;
use PDOException;


class Settings extends Database
{
    public function __construct()
    {
        parent::__construct();
    }

    public function changePassword($id_user, $old_pass, $new_pass)
    {
        $pdo = $this->connect();

        try {
            $sql = "SELECT password = :pass AS thesame FROM ". $this->tabPrefix("users") . " WHERE id_user = :id";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":pass", md5($old_pass), PDO::PARAM_STR);
            $stm->bindParam(":id", $id_user, PDO::PARAM_INT);
            $stm->execute();

            $res = $stm->fetch();

            if($res['thesame'] == 0){
                return -1;
            }

            $pdo->beginTransaction();
            $sql = "UPDATE " . $this->tabPrefix("users") . " SET password = :pass WHERE id_user = :id";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":pass", md5($new_pass), PDO::PARAM_STR);
            $stm->bindParam(":id", $id_user, PDO::PARAM_INT);
            $stm->execute();
            $pdo->commit();
            return true;

        } catch (PDOException $e) {
            $pdo->rollBack();
            $mod = new AppController();
            $mod->throwErrorPage(3, $e);
            return false;
        }
    }

    public function deleteMoneyAccount($id_account, $id_user){
        $pdo = $this->connect();

        try {
            $pdo->beginTransaction();
            $sql = "SELECT money_account_trash FROM ". $this->tabPrefix("user_details") . " WHERE id_user = :id";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":id", $id_user, PDO::PARAM_INT);
            $stm->execute();

            $res = $stm->fetch();

            $res = $res['money_account_trash'];

            $sql = "UPDATE " . $this->tabPrefix("expense") . " SET id_m_acc = :idacc WHERE id_m_acc = :id";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":idacc", $res, PDO::PARAM_INT);
            $stm->bindParam(":id", $id_account, PDO::PARAM_INT);
            $stm->execute();

            $sql = "DELETE FROM " .$this->tabPrefix("money_account") . " WHERE id_m_acc = :id";
            $stm = $pdo->prepare($sql);
            $stm->bindParam(":id", $id_account, PDO::PARAM_INT);
            $stm->execute();

            $pdo->commit();
            return true;

        } catch (PDOException $e) {
            $pdo->rollBack();
            $mod = new AppController();
            $mod->throwErrorPage(3, $e);
            return false;
        }

    }

    public function createMoneyAccount($name, $id_user){
        $pdo = $this->connect();

        try {
            $pdo->beginTransaction();
            $sql = "INSERT INTO ". $this->tabPrefix("money_account") . " (name, id_user, visible) VALUES (:name, :id, 1)";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":name", $name, PDO::PARAM_STR);
            $stm->bindParam(":id", $id_user, PDO::PARAM_INT);
            $stm->execute();

            $pdo->commit();
            return true;

        } catch (PDOException $e) {
            $pdo->rollBack();
            $mod = new AppController();
            $mod->throwErrorPage(3, $e);
            return false;
        }

    }

    public function updateMoneyAccount($id_account, $name){
        $pdo = $this->connect();

        try {
            $pdo->beginTransaction();
            $sql = "UPDATE ". $this->tabPrefix("money_account") . " SET name = :name WHERE id_m_acc = :id";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":name", $name, PDO::PARAM_STR);
            $stm->bindParam(":id", $id_account, PDO::PARAM_INT);
            $stm->execute();

            $pdo->commit();
            return true;

        } catch (PDOException $e) {
            $pdo->rollBack();
            $mod = new AppController();
            $mod->throwErrorPage(3, $e);
            return false;
        }
    }

    public function deleteUser($password, $id_user){
        $pdo = $this->connect();

        try {
            $sql = "SELECT password = :pass AS thesame FROM ". $this->tabPrefix("users") . " WHERE id_user = :id";

            $password = md5($password);

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":pass", $password, PDO::PARAM_STR);
            $stm->bindParam(":id", $id_user, PDO::PARAM_INT);
            $stm->execute();
            $res = $stm->fetch();

            if($res['thesame'] == 0){
                return false;
            }


            $pdo->beginTransaction();
            $sql = "DELETE FROM " . $this->tabPrefix("user_details") . " WHERE id_user = :id";

            $stm = $pdo->prepare($sql);
            $stm->bindParam(":id", $id_user, PDO::PARAM_INT);
            $stm->execute();

            $sql = "SELECT id_m_acc FROM " . $this->tabPrefix("money_account") . " WHERE id_user = :id";
            $sql = "DELETE FROM " . $this->tabPrefix("expense") . " WHERE id_m_acc IN($sql)";
            $stm = $pdo->prepare($sql);
            $stm->bindParam(":id", $id_user, PDO::PARAM_INT);
            $stm->execute();

            $sql = "SELECT id_category FROM " . $this->tabPrefix("category") . " WHERE id_user = :id";
            $sql = "DELETE FROM " . $this->tabPrefix("subcategory") . " WHERE id_category IN($sql)";
            $stm = $pdo->prepare($sql);
            $stm->bindParam(":id", $id_user, PDO::PARAM_INT);
            $stm->execute();

            $sql = "DELETE FROM " . $this->tabPrefix("category") . " WHERE id_user = :id";
            $stm = $pdo->prepare($sql);
            $stm->bindParam(":id", $id_user, PDO::PARAM_INT);
            $stm->execute();

            $sql = "DELETE FROM " . $this->tabPrefix("money_account") . " WHERE id_user = :id";
            $stm = $pdo->prepare($sql);
            $stm->bindParam(":id", $id_user, PDO::PARAM_INT);
            $stm->execute();

            $sql = "DELETE FROM " . $this->tabPrefix("users") . " WHERE id_user = :id";
            $stm = $pdo->prepare($sql);
            $stm->bindParam(":id", $id_user, PDO::PARAM_INT);
            $stm->execute();

            $pdo->commit();
            return true;

        } catch (PDOException $e) {
            $pdo->rollBack();
            $mod = new AppController();
            $mod->throwErrorPage(3, $e);
            return false;
        }
    }

}