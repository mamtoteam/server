<?php
/**
 * Created by PhpStorm.
 * User: cysiu
 * Date: 24.11.2018
 * Time: 15:32
 */

/*
 * Loading extra files
 */

require_once "./routing.php";
require_once "global_params.php";

function autoload_controllers($class_name)
{
    $file = "./Controllers/" . getClassname($class_name) . '.php';
    if (file_exists($file))
        require_once $file;
}

function autoload_models($class_name)
{
    $file = "./Models/" . getClassname($class_name) . '.php';
    if (file_exists($file))
        require_once $file;
}

function getClassname($class_name)
{
    $class_name = explode("\\", $class_name);
    return end($class_name);
}

spl_autoload_register('autoload_controllers');
spl_autoload_register('autoload_models');