<?php
/**
 * Created by PhpStorm.
 * User: cysiu
 * Date: 24.11.2018
 * Time: 19:05
 */

namespace controllers;


use Model\Expenses;
use Model\Login;

class Dashboard extends AppController
{
    public function __construct()
    {
        parent::__construct();
        $this->isLogedInAndMove();
    }

    public function index()
    {
        $model = new Login();
        $modelExpense = new Expenses();
        $user_id = $this->getUserId();

        $username = $model->getUserName($user_id);
        $money = $modelExpense->getMoneyOnAllAccounts($user_id);
        $accounts = $modelExpense->getMoneyAndNamesAccount($user_id);

        $expenses = $modelExpense->getExpenseList($user_id, 10);

        $this->render("index", ['username' => $username, 'money' => $money, 'accounts' => $accounts, 'expenses' => $expenses]);

    }
}