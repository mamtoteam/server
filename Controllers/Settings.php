<?php
/**
 * Created by PhpStorm.
 * User: cysiu
 * Date: 29.12.2018
 * Time: 18:45
 */

namespace controllers;


use Model\Expenses;

class Settings extends AppController
{
    private $model;

    public function __construct()
    {
        parent::__construct();
        $this->model = new \Model\Settings();
        $this->isLogedInAndMove();
    }

    public function index()
    {
        $passInfo = "";
        if(isset($_GET['passChange'])){
            $inf = $_GET['passChange'];
            if($inf == "0")
                $passInfo = "Hasło nie spełnia wymagań";
            elseif ($inf == "2")
                $passInfo = "Podane złe stare hasło";
            elseif ($inf == "1"){
                $passInfo = "Hasło zmieniono poprawnie";
            }

        }

        $infModal = 0;
        $infModalText = "";
        if(isset($_GET['delAcc'])){
            $infModal = 1;
            if($_GET['delAcc'] == true)
                $infModalText = "Błąd podczas usuwania konta! Podano złe hasło!";
            else
                $infModalText = "Błąd podczas usuwania konta!";
        }

        if(isset($_GET['register'])){
            $infModal = 1;
            if($_GET['register'] == "success")
                $infModalText = "Witaj! Znajdujesz sie teraz na podstronie ustawień, możesz tutaj zarządzać swoim kontem i kontami pienięznymi." .
                    "<br>Automatycznie dla Ceibie utworzyliśmy konto Gotówka, ale możesz je usunąć, jak Ci sie nie podoba." .
                    "<br>Stworzyliśmy też Listę kategorii i podkategorii, które możesz wyedytować w edytorze kategorii." .
                    "<br><br>No to zaczynaj!☺";
        }


        $modelExp = new Expenses();
        $accList = $modelExp->getAccountsList($this->getUserId());
        $this->render("index", ['changePasswordInfo' => $passInfo, "accountsList" => $accList, 'infModal' => $infModal, 'infModalTxt' => $infModalText]);
    }

    public function changePassword()
    {
        if (isset($_POST['old_password']) && !EMPTY($_POST['old_password']) &&
            isset($_POST['new_password']) && !EMPTY($_POST['new_password']) &&
            isset($_POST['new_password2']) && !EMPTY($_POST['new_password2'])) {

            if (!$this->changePasswordValidate($_POST['old_password'], $_POST['new_password'], $_POST['new_password2'])) {
                header("Location:?page=settings&passChange=0");
                exit();
            }

            $res = $this->model->changePassword($this->getUserId(), $_POST['old_password'], $_POST['new_password']);
            if ($res === true) {
                header("Location:?page=settings&passChange=1");
            } else {
                header("Location:?page=settings&passChange=2");
            }

        } else {
            $this->throwErrorPage(4, "No recived data");
        }
    }

    private function changePasswordValidate($old_pass, $new_pass, $new_pass2)
    {
        if ($old_pass == $new_pass)
            return false;

        if ($new_pass != $new_pass2)
            return false;

        if (strlen($new_pass) < 6 || strlen($new_pass) > 50)
            return false;

        if (!$this->checkBigLettersInString($new_pass, 1))
            return false;

        if (!$this->checkBigNoumbersInString($new_pass, 1))
            return false;

        return true;
    }

    public function deleteMoneyAccount(){
        if(isset($_GET['id']) && !EMPTY($_GET['id'])){
            $this->model->deleteMoneyAccount($_GET['id'], $this->getUserId());
            header("Location:?page=settings");
        }
        else
            $this->throwErrorPage(4, "No recived data");
    }

    public function ajaxAddMoneyAccount(){
        if(isset($_POST['newAcc']) && !EMPTY($_POST['newAcc'])){
            $this->model->createMoneyAccount($_POST['newAcc'], $this->getUserId());
            header("Location:?page=settings");
        }
        else
            $this->throwErrorPage(4, "No recived data");
    }

    public function ajaxUpdateNameMoneyAccount(){
        if(isset($_GET['name']) && !EMPTY($_GET['name']) && isset($_GET['id_acc']) && !EMPTY($_GET['id_acc'])){
            $this->model->updateMoneyAccount($_GET['id_acc'], $_GET['name']);
            //exit();
            header("Location:?page=settings");
        }
        else
            $this->throwErrorPage(4, "No recived data");
    }

    public function deleteUserAccount(){
        if(isset($_POST['password']) && !EMPTY($_POST['password'])){
            if($this->model->deleteUser($_POST['password'], $this->getUserId())){
                $defContr = new DefaultController();
                $defContr->logout(false);
                header("Location:?page=index&delUser=true");
            }
            else {
                header("Location:?page=settings&delAcc=false");
            }
        }
        else
            $this->throwErrorPage(4, "No recived data");
    }

}