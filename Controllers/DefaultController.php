<?php
/**
 * Created by PhpStorm.
 * User: cysiu
 * Date: 23.10.2018
 * Time: 08:27
 */

namespace controllers;


use Model\Login;

class DefaultController extends AppController
{
    private $model;

    public function __construct()
    {
        parent::__construct();
        $this->model = new Login();

    }

    public function login()
    {
        if ($this->isPost()) {
            $id = $this->model->login($_POST['login-part-email'], $_POST['login-part-password']);
            if ($id != -1) {
                $this->saveLogin($id);
                header("Location:?page=dashboard");
                exit();
            }
        }
        header("Location:?page=index&loginerr=1");
        exit();
    }

    public function logout($moveToIndex = true)
    {
        setcookie('user_id', -1, time() - 100);
        $_SESSION = array();
        session_destroy();
        if ($moveToIndex == true) {
            header("Location:?page=index");
            exit();
        }
    }

    private function saveLogin($id_user)
    {
        setcookie('user_id', $id_user, time() + 3600 * 24 * 7);
        $_SESSION['user_id'] = $id_user;
    }

    public function index()
    {
        $delUserInfo = isset($_GET['delUser']) ? $_GET['delUser'] : "";

        $exist = isset($_GET['register']) ? $_GET['register'] : "";

        $loginerr = isset($_GET['loginerr']) ? $_GET['loginerr'] : 0;
        if ($this->isLogedIn()) {
            header("Location:?page=dashboard");
        } else {
            $this->render("index", ['loginerror' => $loginerr, "delUser" => $delUserInfo, 'exist' => $exist]);
        }
    }

    public function no_page()
    {
        $this->render("../errors/no_page");
    }

    public function error()
    {
        if (ADMIN_ERROR_INFO)
            $err_admin_inf = ISSET($_GET['error_admin']) ? $_GET['error_admin'] : '';
        else
            $err_admin_inf = '';

        $err_inf = isset($_GET['error_code']) ? $this->error_codes[$_GET['error_code']] : '';
        $this->render("../errors/error", ['error_info' => $err_inf, 'error_admin_info' => $err_admin_inf]);
    }

    public function createNewUser()
    {
        if (REGISTER_WITH_EMAIL_CONFIRM)
            $this->createNewUserWithConfirm();
        else
            $this->createNewUserWithoutConfirm();
    }

    private function createNewUserWithoutConfirm()
    {
        if ($this->isPost()) {
            if ($this->validateNewUserData($_POST['register-part-password'], $_POST['register-part-password-repeat'], $_POST['register-part-email'], $_POST['register-part-email-repeat'], $_POST['register-part-username'])) {
                $res = $this->model->addNewUser($_POST['register-part-email'], $_POST['register-part-username'], $_POST['register-part-password']);
                if ($res === false) {
                    header("Location:?page=index&register=exist");
                    exit();
                } else {
                    $this->saveLogin($res);
                    header("Location:?page=settings&register=success");
                    exit();
                }
            } else {
                $this->throwErrorPage(4, "Error in recived data");
            }
        } else
            $this->throwErrorPage(4, "No data recived");
    }

    private function createNewUserWithConfirm()
    {
        $this->throwErrorPage(1, "Function actually not exist.");
    }

    private function validateNewUserData($pass, $passRep, $email, $emailRep, $name)
    {
        if (strlen($pass) < 6 || strlen($pass) > 50)
            return false;

        if ($pass != $passRep)
            return false;

        if (!$this->checkBigLettersInString($pass, 1))
            return false;

        if (!$this->checkBigNoumbersInString($pass, 1))
            return false;

        if (strlen($email) > 50 || strlen($email) < 5)
            return false;

        if ($email != $emailRep)
            return false;

        if (!filter_var($email, FILTER_VALIDATE_EMAIL))
            return false;

        if (strlen($name) < 3 || strlen($name) > 50)
            return false;

        return true;
    }
}